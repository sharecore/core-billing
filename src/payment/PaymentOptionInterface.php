<?php

namespace core\billing\payment;

use core\billing\invoice\InvoiceInterface;

interface PaymentOptionInterface
{
    public const METHOD_GET = 'GET';
    public const METHOD_POST = 'POST';

    /**
     * @param string $name
     * @return PaymentOptionInterface
     */
    public function setName(string $name): PaymentOptionInterface;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $action
     * @return PaymentOptionInterface
     */
    public function setAction(string $action): PaymentOptionInterface;

    /**
     * @return string
     */
    public function getAction(): string;

    /**
     * @param string $method
     * @return PaymentOptionInterface
     */
    public function setMethod(string $method): PaymentOptionInterface;

    /**
     * @return string
     */
    public function getMethod(): string;

    /**
     * @param array $params
     * @return PaymentOptionInterface
     */
    public function setParams(array $params): PaymentOptionInterface;

    /**
     * @return array
     */
    public function getParams(): array;

    /**
     * @param $config
     * @param InvoiceInterface $invoice
     * @return PaymentOptionInterface
     */
     public function build($config, InvoiceInterface $invoice): PaymentOptionInterface;
}
