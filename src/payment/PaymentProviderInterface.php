<?php

namespace core\billing\payment;

use core\billing\invoice\InvoiceInterface;

interface PaymentProviderInterface
{
    /**
     * @param array $properties
     * @return PaymentProviderInterface
     */
    public function loadResponse(array $properties): PaymentProviderInterface;

    /**
     * @return bool
     */
    public function checkResponse(): bool;

    /**
     * @return int
     */
    public function getInvoiceId(): int;

    /**
     * @return float
     */
    public function getAmount(): float;

    /**
     * @param InvoiceInterface $invoice
     * @return array
     */
    public function getPayOptions(InvoiceInterface $invoice): array;
}
