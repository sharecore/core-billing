<?php

namespace core\billing\payment;

use core\billing\invoice\InvoiceInterface;

abstract class PaymentOptionAbstract implements PaymentOptionInterface
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $action;

    /**
     * @var string
     */
    public $method;

    /**
     * @var array
     */
    public $params = [];

    /**
     * @param $config
     * @param InvoiceInterface $invoice
     */
    public function __construct($config, InvoiceInterface $invoice)
    {
        $this->build($config, $invoice);
    }

    /**
     * @param string $name
     * @return PaymentOptionInterface
     */
    public function setName(string $name): PaymentOptionInterface
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $action
     * @return PaymentOptionInterface
     */
    public function setAction(string $action): PaymentOptionInterface
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $method
     * @return PaymentOptionInterface
     */
    public function setMethod(string $method): PaymentOptionInterface
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param array $params
     * @return PaymentOptionInterface
     */
    public function setParams(array $params): PaymentOptionInterface
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}
