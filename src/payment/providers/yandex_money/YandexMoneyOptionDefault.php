<?php

namespace core\billing\payment\providers\yandex_money;

use core\billing\invoice\InvoiceInterface;
use core\billing\payment\PaymentOptionAbstract;
use core\billing\payment\PaymentOptionInterface;

class YandexMoneyOptionDefault extends PaymentOptionAbstract
{
    /**
     * @param YandexMoneyConfig $config
     * @param InvoiceInterface $invoice
     * @return PaymentOptionAbstract
     */
    public function build($config, InvoiceInterface $invoice): PaymentOptionInterface
    {
        return $this
            ->setName('Yandex.Money')
            ->setMethod(PaymentOptionAbstract::METHOD_GET)
            ->setAction($config->payUrl)
            ->setParams([
                'label' => $invoice->getId(),
                'receiver' => $config->receiver,
                'quickpay-form' => 'shop',
                'is-inner-form' => 'true',
                'targets' => $invoice->getDescription(),
                'title' => 'true',
                'sum' => $invoice->getTotalPrice(),
                'successURL' => $config->successURL,
                'quickpay-back-url' => $config->backURL,
                'paymentType' => 'PC'
            ]);
    }
}
