<?php

namespace core\billing\payment\providers\yandex_money;

class YandexMoneyConfig
{
    /**
     * @var string
     */
    public $payUrl = 'https://money.yandex.ru/quickpay/confirm.xml';

    /**
     * @var string
     */
    public $secret;

    /**
     * @var string
     */
    public $receiver;

    /**
     * @var string
     */
    public $successURL;

    /**
     * @var string
     */
    public $backURL;
}