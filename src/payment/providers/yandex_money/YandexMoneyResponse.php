<?php

namespace core\billing\payment\providers\yandex_money;

class YandexMoneyResponse
{
    /**
     * @var int
     */
    public $operation_id;

    /**
     * @var string
     */
    public $notification_type;

    /**
     * @var string
     */
    public $datetime;

    /**
     * @var string
     */
    public $sha1_hash;

    /**
     * @var int
     */
    public $sender;

    /**
     * @var string
     */
    public $codepro;

    /**
     * @var int
     */
    public $currency;

    /**
     * @var float
     */
    public $amount;

    /**
     * @var float
     */
    public $withdraw_amount;

    /**
     * @var string
     */
    public $label;
    /**
     * @var string
     */
    public $lastname;

    /**
     * @var string
     */
    public $firstname;

    /**
     * @var string
     */
    public $fathersname;

    /**
     * @var int
     */
    public $zip;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $street;

    /**
     * @var int
     */
    public $building;

    /**
     * @var int
     */
    public $suite;

    /**
     * @var int
     */
    public $flat;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $email;

    /**
     * @var bool
     */
    public $unaccepted;

    /**
     * @var string
     */
    public $operation_label;
}
