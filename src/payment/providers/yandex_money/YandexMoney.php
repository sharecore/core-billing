<?php

namespace core\billing\payment\providers\yandex_money;

use core\billing\payment\PaymentProviderAbstract;

/**
 * @property YandexMoneyConfig $config
 * @property YandexMoneyResponse $response
 */
class YandexMoney extends PaymentProviderAbstract
{
    /**
     * @var string
     */
    public $configClass = YandexMoneyConfig::class;

    /**
     * @var string
     */
    public $responseClass = YandexMoneyResponse::class;

    /**
     * @var array
     */
    public $payOptionClasses = [
        YandexMoneyOptionDefault::class,
        YandexMoneyOptionVisa::class
    ];

    /**
     * @return bool
     */
    public function checkResponse(): bool
    {
        $data = [
            $this->response->notification_type,
            $this->response->operation_id,
            $this->response->amount,
            $this->response->currency,
            $this->response->datetime,
            $this->response->sender,
            $this->response->codepro,
            $this->config->secret,
            $this->response->label,
        ];

        return $this->response->sha1_hash === sha1(implode('&', $data));
    }

    /**
     * @return int
     */
    public function getInvoiceId(): int
    {
        return (integer)$this->response->label;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->response->withdraw_amount;
    }
}
