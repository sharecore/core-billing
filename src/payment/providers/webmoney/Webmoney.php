<?php

namespace core\billing\payment\providers\webmoney;

use core\billing\payment\PaymentProviderAbstract;
use core\billing\payment\providers\yandex_money\WebmoneyOptionDefault;

/**
 * @property WebmoneyConfig $config
 * @property WebmoneyResponse $response
 */
class Webmoney extends PaymentProviderAbstract
{
    /**
     * @var string
     */
    public $configClass = WebmoneyConfig::class;

    /**
     * @var string
     */
    public $responseClass = WebmoneyResponse::class;

    /**
     * @var array
     */
    public $payOptionClasses = [
        WebmoneyOptionDefault::class,
    ];

    /**
     * @return bool
     */
    public function checkResponse(): bool
    {
        $data = [
            $this->response->LMI_PAYEE_PURSE,
            $this->response->LMI_PAYMENT_AMOUNT,
            $this->response->LMI_PAYMENT_NO,
            $this->response->LMI_MODE,
            $this->response->LMI_SYS_INVS_NO,
            $this->response->LMI_SYS_TRANS_NO,
            $this->response->LMI_SYS_TRANS_DATE,
            $this->config->secret,
            $this->response->LMI_PAYER_PURSE,
            $this->response->LMI_PAYER_WM,
        ];

        return $this->response->LMI_HASH2 === strtoupper(hash('sha256', implode(';', $data)));
    }

    /**
     * @return int
     */
    public function getInvoiceId(): int
    {
        return $this->response->LMI_PAYMENT_NO;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return round($this->response->LMI_PAYMENT_AMOUNT);
    }
}
