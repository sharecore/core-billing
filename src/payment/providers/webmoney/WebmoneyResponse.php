<?php

namespace core\billing\payment\providers\webmoney;

class WebmoneyResponse
{
    /**
     * @var int
     */
    public $LMI_MODE;

    /**
     * @var float
     */
    public $LMI_PAYMENT_AMOUNT;

    /**
     * @var float
     */
    public $LMI_HOLD;

    /**
     * @var string
     */
    public $LMI_PAYEE_PURSE;

    /**
     * @var int
     */
    public $LMI_PAYMENT_NO;

    /**
     * @var int
     */
    public $LMI_PAYER_WM;

    /**
     * @var string
     */
    public $LMI_PAYER_PURSE;

    /**
     * @var string
     */
    public $LMI_PAYER_COUNTRYID;

    /**
     * @var string
     */
    public $LMI_PAYER_PCOUNTRYID;

    /**
     * @var string
     */
    public $LMI_PAYER_IP;

    /**
     * @var int
     */
    public $LMI_SYS_INVS_NO;

    /**
     * @var int
     */
    public $LMI_SYS_TRANS_NO;

    /**
     * @var string
     */
    public $LMI_SYS_TRANS_DATE;

    /**
     * @var string
     */
    public $LMI_HASH;

    /**
     * @var string
     */
    public $LMI_HASH2;

    /**
     * @var string
     */
    public $LMI_PAYMENT_DESC;
    /**
     * @var string
     */
    public $LMI_LANG;

    /**
     * @var string
     */
    public $LMI_DBLCHK;
}
