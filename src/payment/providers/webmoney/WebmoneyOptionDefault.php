<?php

namespace core\billing\payment\providers\yandex_money;

use core\billing\invoice\InvoiceInterface;
use core\billing\payment\PaymentOptionAbstract;
use core\billing\payment\PaymentOptionInterface;
use core\billing\payment\providers\webmoney\WebmoneyConfig;

class WebmoneyOptionDefault extends PaymentOptionAbstract
{
    /**
     * @param WebmoneyConfig $config
     * @param InvoiceInterface $invoice
     * @return PaymentOptionAbstract
     */
    public function build($config, InvoiceInterface $invoice): PaymentOptionInterface
    {
        return $this
            ->setName('Webmoney')
            ->setMethod(PaymentOptionInterface::METHOD_POST)
            ->setAction($config->payUrl)
            ->setParams([
                'LMI_PAYMENT_AMOUNT' => $invoice->getTotalPrice(),
                'LMI_PAYMENT_NO' => $invoice->getId(),
                'LMI_PAYMENT_DESC' => $invoice->getDescription(),
                'LMI_PAYMENT_DESC_BASE64' => base64_encode($invoice->getDescription()),
                'LMI_PAYEE_PURSE' => $config->receiver,
                'LMI_SIM_MODE' => 0,
            ]);
    }
}
