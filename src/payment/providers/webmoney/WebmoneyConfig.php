<?php

namespace core\billing\payment\providers\webmoney;

class WebmoneyConfig
{
    /**
     * @var string
     */
    public $payUrl = 'https://merchant.webmoney.ru/lmi/payment.asp';

    /**
     * @var string
     */
    public $secret;

    /**
     * @var string
     */
    public $receiver;
}
