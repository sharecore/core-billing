<?php

namespace core\billing\payment\providers\robokassa;

use core\billing\payment\PaymentProviderAbstract;
use core\billing\payment\providers\yandex_money\RobokassaOptionDefault;

/**
 * @property RobokassaConfig $config
 * @property RobokassaResponse $response
 */
class Robokassa extends PaymentProviderAbstract
{
    /**
     * @var string
     */
    public $configClass = RobokassaConfig::class;

    /**
     * @var string
     */
    public $responseClass = RobokassaResponse::class;

    /**
     * @var array
     */
    public $payOptionClasses = [
        RobokassaOptionDefault::class,
    ];

    /**
     * @return bool
     */
    public function checkResponse(): bool
    {
        $keyArray = [
            [
                $this->response->InvId,
                $this->response->OutSum,
                $this->config->pass1,
            ],
            [
                $this->response->InvId,
                $this->response->OutSum,
                $this->config->pass2,
            ]
        ];

        foreach ($keyArray as $keys) {
            if ($this->response->SignatureValue !== md5(implode(':', $keys))) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return int
     */
    public function getInvoiceId(): int
    {
        return $this->response->InvId;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->response->OutSum;
    }
}
