<?php

namespace core\billing\payment\providers\yandex_money;

use core\billing\invoice\InvoiceInterface;
use core\billing\payment\PaymentOptionAbstract;
use core\billing\payment\PaymentOptionInterface;
use core\billing\payment\providers\robokassa\RobokassaConfig;

class RobokassaOptionDefault extends PaymentOptionAbstract
{
    /**
     * @param RobokassaConfig $config
     * @param InvoiceInterface $invoice
     * @return PaymentOptionAbstract
     */
    public function build($config, InvoiceInterface $invoice): PaymentOptionInterface
    {
        $keys = [
            $config->login,
            $invoice->getTotalPrice(),
            $invoice->getId(),
            $config->pass1,
        ];

        return $this
            ->setName('Robokassa')
            ->setMethod(PaymentOptionInterface::METHOD_GET)
            ->setAction($config->payUrl)
            ->setParams([
                'MrchLogin' => $config->login,
                'OutSum' => $invoice->getTotalPrice(),
                'InvId' => $invoice->getId(),
                'Desc' => $invoice->getDescription(),
                'SignatureValue' => md5(implode(':', $keys)),
                'Email' => $invoice->getEmail(),
                'Culture' => $config->culture,
                'isTest' => $config->isTest,
            ]);
    }
}
