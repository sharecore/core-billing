<?php

namespace core\billing\payment\providers\robokassa;

class RobokassaResponse
{
    /**
     * @var int
     */
    public $InvId;

    /**
     * @var float
     */
    public $OutSum;

    /**
     * @var string
     */
    public $SignatureValue;

    /**
     * @var string
     */
    public $Culture;
}
