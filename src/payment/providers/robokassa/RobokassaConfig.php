<?php

namespace core\billing\payment\providers\robokassa;

class RobokassaConfig
{
    /**
     * @var string
     */
    public $payUrl = 'http://auth.robokassa.ru/Merchant/Index.aspx';

    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $pass1;

    /**
     * @var string
     */
    public $pass2;

    /**
     * @var bool
     */
    public $isTest = false;

    /**
     * @var string
     */
    public $culture = 'ru';
}
