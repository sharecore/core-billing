<?php

namespace core\billing\payment;

class BaseFactory
{
    /**
     * @param string $class
     * @param array|null $properties
     * @return mixed
     */
    public static function create(string $class, ?array $properties = null)
    {
        $object = new $class();

        if ($properties) {
            foreach ($properties as $name => $value) {
                if (property_exists($class, $name)) {
                    $object->$name = $value;
                }
            }
        }

        return $object;
    }
}
