<?php

namespace core\billing\payment;

use core\billing\invoice\InvoiceInterface;

abstract class PaymentProviderAbstract implements PaymentProviderInterface
{
    /**
     * @var string
     */
    public $configClass;

    /**
     * @var string
     */
    public $responseClass;

    /**
     * @var array
     */
    public $payOptionClasses;

    /**
     * @var mixed
     */
    protected $config;

    /**
     * @var mixed
     */
    protected $response;

    /**
     * @param array $properties
     */
    public function __construct(array $properties)
    {
        $this->config = BaseFactory::create($this->configClass, $properties);
    }

    /**
     * @param array $properties
     * @return PaymentProviderInterface
     */
    public function loadResponse(array $properties): PaymentProviderInterface
    {
        $this->response = BaseFactory::create($this->responseClass, $properties);
        return $this;
    }

    /**
     * @param InvoiceInterface $invoice
     * @return array
     */
    public function getPayOptions(InvoiceInterface $invoice): array
    {
        $options = [];
        foreach ($this->payOptionClasses as $payOptionClass) {
            $options[] = new $payOptionClass($this->config, $invoice);
        }
        return $options;
    }
}
