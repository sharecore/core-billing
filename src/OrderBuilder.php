<?php

namespace core\billing;

use core\billing\item\ItemInterface;
use core\billing\order\OrderInterface;

class OrderBuilder
{
    /**
     * @var ItemInterface[]
     */
    private $items = [];

    /**
     * @param array $classNames
     */
    public function buildOrders(array $classNames): void
    {
        foreach ($classNames as $className) {
            $this->buildOrder($className);
        }
    }

    /**
     * @param string $className
     */
    public function buildOrder(string $className): void
    {
        /** @var OrderInterface $section */
        $section = new $className;
        $this->addItem($section->build());
    }

    /**
     * @param ItemInterface $section
     */
    public function addItem(ItemInterface $section): void
    {
        $this->items[] = $section;
    }

    /**
     * @return ItemInterface[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}