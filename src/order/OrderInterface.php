<?php

namespace core\billing\order;

use core\billing\item\ItemInterface;

interface OrderInterface
{
    /**
     * @return ItemInterface|null
     */
    public function build(): ?ItemInterface;
}