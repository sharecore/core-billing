<?php

namespace core\billing\helper;

use core\billing\item\ItemInterface;
use core\billing\item\ItemPurchaseInterface;

class PurchaseHelper
{
    /**
     * @param ItemInterface[]|ItemPurchaseInterface[] $items
     * @param int $offerId
     * @param int $purchaseId
     * @return ItemPurchaseInterface|null
     */
    public static function findPurchase($items, int $offerId, int $purchaseId): ?ItemPurchaseInterface
    {
        foreach ($items as $item) {
            if ($item instanceof ItemPurchaseInterface && $item->getOfferId() === $offerId && $item->getId() === $purchaseId) {
                return $item;
            }
            if ($find = self::findPurchase($item->getChild(), $offerId, $purchaseId)) {
                return $find;
            }
        }
        return null;
    }

    /**
     * @param int $days
     * @return string|null
     */
    public static function getFinishedAtByDays(?int $days): ?string
    {
        return $days ? DateHelper::now('+' . $days . 'days', 'Y-m-d 00:00:00') : null;
    }

    /**
     * @param ItemPurchaseInterface $purchase
     * @return float|null
     */
    public static function getPriceLeft(ItemPurchaseInterface $purchase): ?float
    {
        $pricePerDay = self::getPricePerDay($purchase);
        $daysLeft = self::getDaysLeft($purchase);

        if ($pricePerDay && $daysLeft) {
            return $pricePerDay * $daysLeft;
        }

        return null;
    }

    /**
     * @param ItemPurchaseInterface $purchase
     * @return float|null
     */
    public static function getPricePerDay(ItemPurchaseInterface $purchase): ?float
    {
        $price = $purchase->getPrice();
        $days = self::getDays($purchase);

        if ($price && $days) {
            return $price / $days;
        }

        return null;
    }

    /**
     * @param ItemPurchaseInterface $purchase
     * @return int|null
     */
    public static function getDaysLeft(ItemPurchaseInterface $purchase): ?int
    {
        if ($purchase->getFinishedAt()) {
            return DateHelper::dateDiffDays(self::getStartedAtNow(), $purchase->getFinishedAt());
        }

        return null;
    }

    /**
     * @return string
     */
    public static function getStartedAtNow(): string
    {
        return DateHelper::now(null, 'Y-m-d 00:00:00');
    }

    /**
     * @param ItemPurchaseInterface $purchase
     * @return int|null
     */
    public static function getDays(ItemPurchaseInterface $purchase): ?int
    {
        if ($purchase->getDays()) {
            return $purchase->getDays();
        }

        if ($purchase->getStartedAt() && $purchase->getFinishedAt()) {
            return DateHelper::dateDiffDays($purchase->getStartedAt(), $purchase->getFinishedAt());
        }

        return self::getDaysByPeriod($purchase->getPeriod());
    }

    /**
     * @param int $period
     * @return int|null
     */
    private static function getDaysByPeriod(int $period): ?int
    {
        return ItemPurchaseInterface::DAYS_ARRAY[$period] ?: null;
    }
}


