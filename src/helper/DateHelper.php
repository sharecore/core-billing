<?php

namespace core\billing\helper;

use DateInterval;

class DateHelper
{
    /**
     * @param string|null $diff
     * @param string $format
     * @return string
     */
    public static function now(string $diff = null, $format = 'Y-m-d H:i:s'): string
    {
        if ($diff) {
            $time = strtotime(gmdate($format) . ' ' . $diff);
            return date($format, $time);
        }
        return gmdate($format);
    }

    /**
     * @param string $one
     * @param string $two
     * @return DateInterval
     */
    public static function dateDiff(string $one, string $two): DateInterval
    {
        $date_one = date_create($one);
        $date_two = date_create($two);
        return date_diff($date_one, $date_two);
    }

    /**
     * @param string $one
     * @param string $two
     * @return int
     */
    public static function dateDiffDays(string $one, string $two): int
    {
        return self::dateDiff($one, $two)->days;
    }
}
