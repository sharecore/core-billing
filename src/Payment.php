<?php

namespace core\billing;

use core\billing\invoice\InvoiceInterface;
use core\billing\payment\PaymentProviderInterface;

class Payment implements PaymentProviderInterface
{
    /**
     * @var PaymentProviderInterface
     */
    private $provider;

    /**
     * @param string $providerClass
     * @param array $properties
     */
    public function __construct(string $providerClass, array $properties)
    {
        $this->provider = new $providerClass($properties);
    }

    /**
     * @param array $params
     * @return PaymentProviderInterface
     */
    public function loadResponse(array $params): PaymentProviderInterface
    {
        $this->provider->loadResponse($params);
        return $this;
    }

    /**
     * @return bool
     */
    public function checkResponse(): bool
    {
        return $this->provider->checkResponse();
    }

    /**
     * @return int
     */
    public function getInvoiceId(): int
    {
        return $this->provider->getInvoiceId();
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->provider->getAmount();
    }

    /**
     * @param InvoiceInterface $invoice
     * @return array
     */
    public function getPayOptions(InvoiceInterface $invoice): array
    {
        return $this->provider->getPayOptions($invoice);
    }
}