<?php

namespace core\billing;

use core\billing\item\ItemPurchaseInterface;
use core\billing\purchase\PurchaseStrategyInterface;

class PurchaseBuilder
{
    /**
     * @var PurchaseStrategyInterface
     */
    private $strategy;

    /**
     * @param ItemPurchaseInterface $purchase
     * @param string $purchaseStrategyClass
     */
    public function __construct(ItemPurchaseInterface $purchase, string $purchaseStrategyClass)
    {
        $this->strategy = new $purchaseStrategyClass($purchase);
    }

    /**
     * @param ItemPurchaseInterface $purchase
     * @return PurchaseBuilder
     */
    public function setNeedPurchase(ItemPurchaseInterface $purchase): PurchaseBuilder
    {
        $this->strategy->setNeedPurchase($purchase);
        return $this;
    }

    /**
     * @param ItemPurchaseInterface $purchase
     * @return PurchaseBuilder
     */
    public function setCurrentPurchase(ItemPurchaseInterface $purchase): PurchaseBuilder
    {
        $this->strategy->setCurrentPurchase($purchase);
        return $this;
    }

    /**
     * @return bool
     */
    public function canPurchase(): bool
    {
        return $this->strategy->canPurchase();
    }

    /**
     * @return ItemPurchaseInterface
     */
    public function build(): ItemPurchaseInterface
    {
        return $this->strategy->build();
    }
}
