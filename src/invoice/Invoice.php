<?php

namespace core\billing\invoice;

class Invoice implements InvoiceInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var float
     */
    protected $total_price;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $email;

    /**
     * @param int $id
     * @return InvoiceInterface
     */
    public function setId(int $id): InvoiceInterface
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param float $total_price
     * @return InvoiceInterface
     */
    public function setTotalPrice(float $total_price): InvoiceInterface
    {
        $this->total_price = $total_price;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->total_price;
    }

    /**
     * @param string $description
     * @return InvoiceInterface
     */
    public function setDescription(string $description): InvoiceInterface
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $email
     * @return InvoiceInterface
     */
    public function setEmail(string $email): InvoiceInterface
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}
