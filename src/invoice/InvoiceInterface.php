<?php

namespace core\billing\invoice;

interface InvoiceInterface
{
    /**
     * @param int $id
     * @return InvoiceInterface
     */
    public function setId(int $id): InvoiceInterface;

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param float $total_price
     * @return InvoiceInterface
     */
    public function setTotalPrice(float $total_price): InvoiceInterface;

    /**
     * @return float
     */
    public function getTotalPrice(): float;

    /**
     * @param string $description
     * @return InvoiceInterface
     */
    public function setDescription(string $description): InvoiceInterface;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @param string $email
     * @return InvoiceInterface
     */
    public function setEmail(string $email): InvoiceInterface;

    /**
     * @return string
     */
    public function getEmail(): string;
}
