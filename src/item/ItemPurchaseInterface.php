<?php

namespace core\billing\item;

interface ItemPurchaseInterface
{
    public const PURCHASE_ID_BUY = 1;
    public const PURCHASE_ID_ADDITIONAL = 2;
    public const PURCHASE_ID_TRANSITION = 3;

    public const PERIOD_UNLIMITED = 0;
    public const PERIOD_MONTH = 1;
    public const PERIOD_3_MONTHS = 3;
    public const PERIOD_6_MONTHS = 6;
    public const PERIOD_12_MONTHS = 12;

    PUBLIC CONST CURRENCY_RUB = 1;
    PUBLIC CONST CURRENCY_USD = 2;
    PUBLIC CONST CURRENCY_EUR = 3;

    public const DAYS_ARRAY = [
        self::PERIOD_UNLIMITED => 0,
        self::PERIOD_MONTH => 30,
        self::PERIOD_3_MONTHS => 90,
        self::PERIOD_6_MONTHS => 180,
        self::PERIOD_12_MONTHS => 360,
    ];

    /**
     * @param int $id
     * @return ItemPurchaseInterface
     */
    public function setId(int $id): ItemPurchaseInterface;

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return int
     */
    public function getSectionId(): int;

    /**
     * @param int $value
     * @return ItemPurchaseInterface
     */
    public function setValue(int $value): ItemPurchaseInterface;

    /**
     * @param int $unitId
     * @return ItemPurchaseInterface
     */
    public function setUnitId(int $unitId): ItemPurchaseInterface;

    /**
     * @return int
     */
    public function getUnitId(): int;

    /**
     * @param int $offerId
     * @return ItemPurchaseInterface
     */
    public function setOfferId(int $offerId): ItemPurchaseInterface;

    /**
     * @return int
     */
    public function getOfferId(): int;

    /**
     * @param int $sectionId
     * @return ItemPurchaseInterface
     */
    public function setSectionId(int $sectionId): ItemPurchaseInterface;

    /**
     * @return int
     */
    public function getValue(): int;

    /**
     * @param string|null $startedAt
     * @return ItemPurchaseInterface
     */
    public function setStartedAt(?string $startedAt): ItemPurchaseInterface;

    /**
     * @return string|null
     */
    public function getStartedAt(): ?string;

    /**
     * @param string|null $finishedAt
     * @return ItemPurchaseInterface
     */
    public function setFinishedAt(?string $finishedAt): ItemPurchaseInterface;

    /**
     * @return string|null
     */
    public function getFinishedAt(): ?string;

    /**
     * @param int|null $period
     * @return ItemPurchaseInterface
     */
    public function setPeriod(?int $period): ItemPurchaseInterface;

    /**
     * @return int|null
     */
    public function getPeriod(): ?int;

    /**
     * @param int|null $days
     * @return ItemPurchase
     */
    public function setDays(?int $days): ItemPurchase;

    /**
     * @return int|null
     */
    public function getDays(): ?int;

    /**
     * @param string|null $name
     * @return ItemPurchaseInterface
     */
    public function setName(?string $name): ItemPurchaseInterface;

    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @param string|null $description
     * @return ItemPurchaseInterface
     */
    public function setDescription(?string $description): ItemPurchaseInterface;

    /**
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * @param float|null $price
     * @return ItemPurchaseInterface
     */
    public function setPrice(?float $price): ItemPurchaseInterface;

    /**
     * @return float|null
     */
    public function getPrice(): ?float;

    /**
     * @param int $currency
     * @return ItemPurchaseInterface
     */
    public function setCurrency(int $currency): ItemPurchaseInterface;

    /**
     * @return int
     */
    public function getCurrency(): int;

    /**
     * @param float|null $priceTransition
     * @return ItemPurchaseInterface
     */
    public function setPriceTransition(?float $priceTransition): ItemPurchaseInterface;

    /**
     * @return float|null
     */
    public function getPriceTransition(): ?float;

    /**
     * @param float|null $priceWithoutDiscount
     * @return ItemPurchaseInterface
     */
    public function setPriceWithoutDiscount(?float $priceWithoutDiscount): ItemPurchaseInterface;

    /**
     * @return float|null
     */
    public function getPriceWithoutDiscount(): ?float;

    /**
     * @param int $sale
     * @return ItemPurchaseInterface
     */
    public function setDiscount(?int $sale): ItemPurchaseInterface;

    /**
     * @return int
     */
    public function getDiscount(): ?int;

}
