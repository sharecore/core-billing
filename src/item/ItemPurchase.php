<?php

namespace core\billing\item;

class ItemPurchase extends AbstractItem implements ItemPurchaseInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $sectionId;

    /**
     * @var int
     */
    public $unitId;

    /**
     * @var int
     */
    public $offerId;

    /**
     * @var int
     */
    public $value;

    /**
     * @var string|null
     */
    public $startedAt;

    /**
     * @var string|null
     */
    public $finishedAt;

    /**
     * @var int|null
     */
    public $period;

    /**
     * @var int|null
     */
    public $days;

    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $description;

    /**
     * @var float|null
     */
    public $price;

    /**
     * @var int
     */
    public $currency;

    /**
     * @var float|null
     */
    public $priceTransition;

    /**
     * @var float|null
     */
    public $priceWithoutDiscount;

    /**
     * @var int
     */
    public $discount;

    /**
     * @param int $id
     * @return ItemPurchaseInterface
     */
    public function setId(int $id): ItemPurchaseInterface
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $sectionId
     * @return ItemPurchaseInterface
     */
    public function setSectionId(int $sectionId): ItemPurchaseInterface
    {
        $this->sectionId = $sectionId;
        return $this;
    }

    /**
     * @return int
     */
    public function getSectionId(): int
    {
        return $this->sectionId;
    }

    /**
     * @param int $unitId
     * @return ItemPurchaseInterface
     */
    public function setUnitId(int $unitId): ItemPurchaseInterface
    {
        $this->unitId = $unitId;
        return $this;
    }

    /**
     * @return int
     */
    public function getUnitId(): int
    {
        return $this->unitId;
    }

    /**
     * @param int $offerId
     * @return ItemPurchaseInterface
     */
    public function setOfferId(int $offerId): ItemPurchaseInterface
    {
        $this->offerId = $offerId;
        return $this;
    }

    /**
     * @return int
     */
    public function getOfferId(): int
    {
        return $this->offerId;
    }

    /**
     * @param int $value
     * @return ItemPurchaseInterface
     */
    public function setValue(int $value): ItemPurchaseInterface
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param string|null $startedAt
     * @return ItemPurchaseInterface
     */
    public function setStartedAt(?string $startedAt): ItemPurchaseInterface
    {
        $this->startedAt = $startedAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartedAt(): ?string
    {
        return $this->startedAt;
    }

    /**
     * @param string|null $finishedAt
     * @return ItemPurchaseInterface
     */
    public function setFinishedAt(?string $finishedAt): ItemPurchaseInterface
    {
        $this->finishedAt = $finishedAt;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFinishedAt(): ?string
    {
        return $this->finishedAt;
    }

    /**
     * @param int $period |null
     * @return ItemPurchaseInterface
     */
    public function setPeriod(?int $period): ItemPurchaseInterface
    {
        $this->period = $period;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPeriod(): ?int
    {
        return $this->period;
    }

    /**
     * @param int|null $days
     * @return ItemPurchase
     */
    public function setDays(?int $days): ItemPurchase
    {
        $this->days = $days;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDays(): ?int
    {
        return $this->days;
    }

    /**
     * @param string|null $name
     * @return ItemPurchaseInterface
     */
    public function setName(?string $name): ItemPurchaseInterface
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $description
     * @return ItemPurchaseInterface
     */
    public function setDescription(?string $description): ItemPurchaseInterface
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param float|null $price
     * @return ItemPurchaseInterface
     */
    public function setPrice(?float $price): ItemPurchaseInterface
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param int $currency
     * @return ItemPurchaseInterface
     */
    public function setCurrency(int $currency): ItemPurchaseInterface
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrency(): int
    {
        return $this->currency;
    }

    /**
     * @param float|null $priceTransition
     * @return ItemPurchase
     */
    public function setPriceTransition(?float $priceTransition): ItemPurchaseInterface
    {
        $this->priceTransition = $priceTransition;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPriceTransition(): ?float
    {
        return $this->priceTransition;
    }

    /**
     * @param int $discount
     * @return ItemPurchaseInterface
     */
    public function setDiscount(?int $discount): ItemPurchaseInterface
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return int
     */
    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    /**
     * @param float|null $priceWithoutDiscount
     * @return ItemPurchaseInterface
     */
    public function setPriceWithoutDiscount(?float $priceWithoutDiscount): ItemPurchaseInterface
    {
        $this->priceWithoutDiscount = $priceWithoutDiscount;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPriceWithoutDiscount(): ?float
    {
        return $this->priceWithoutDiscount;
    }
}
