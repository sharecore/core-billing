<?php

namespace core\billing\item;

interface ItemInterface
{
    /**
     * @param ItemInterface $parent
     * @return ItemInterface
     */
    public function setParent(ItemInterface $parent): ItemInterface;

    /**
     * @return ItemInterface
     */
    public function getParent(): ItemInterface;

    /**
     * @param ItemInterface $section
     * @return ItemInterface
     */
    public function addChild(ItemInterface $section): ItemInterface;

    /**
     * @return ItemInterface[]
     */
    public function getChild(): array;
}
