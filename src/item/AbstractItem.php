<?php

namespace core\billing\item;

class AbstractItem implements ItemInterface
{
    /**
     * @var ItemInterface
     */
    private $parent;

    /**
     * @var ItemInterface[]
     */
    protected $child = [];

    /**
     * @param ItemInterface $parent
     * @return ItemInterface
     */
    public function setParent(ItemInterface $parent): ItemInterface
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return ItemInterface
     */
    public function getParent(): ItemInterface
    {
        return $this->parent;
    }

    /**
     * @param ItemInterface $section
     * @return AbstractItem
     */
    public function addChild(ItemInterface $section): ItemInterface
    {
        $section->setParent($this);
        $this->child[] = $section;
        return $this;
    }

    /**
     * @return AbstractItem[]
     */
    public function getChild(): array
    {
        return $this->child;
    }
}
