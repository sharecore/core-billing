<?php

namespace core\billing\purchase;

use core\billing\item\ItemPurchaseInterface;

interface PurchaseStrategyInterface
{
    /**
     * @param ItemPurchaseInterface $purchase
     */
    public function __construct(ItemPurchaseInterface $purchase);

    /**
     * @param ItemPurchaseInterface $purchase
     * @return PurchaseStrategyInterface
     */
    public function setPurchase(ItemPurchaseInterface $purchase): PurchaseStrategyInterface;

    /**
     * @return ItemPurchaseInterface
     */
    public function getPurchase(): ItemPurchaseInterface;

    /**
     * @param ItemPurchaseInterface $purchase
     * @return PurchaseStrategyInterface
     */
    public function setNeedPurchase(ItemPurchaseInterface $purchase): PurchaseStrategyInterface;

    /**
     * @return ItemPurchaseInterface
     */
    public function getNeedPurchase(): ItemPurchaseInterface;

    /**
     * @param ItemPurchaseInterface $purchase
     * @return PurchaseStrategyInterface
     */
    public function setCurrentPurchase(ItemPurchaseInterface $purchase): PurchaseStrategyInterface;

    /**
     * @return ItemPurchaseInterface
     */
    public function getCurrentPurchase(): ?ItemPurchaseInterface;

    /**
     * @return bool
     */
    public function canPurchase(): bool;

    /**
     * @return ItemPurchaseInterface
     */
    public function build(): ItemPurchaseInterface;
}
