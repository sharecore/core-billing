<?php

namespace core\billing\purchase;

use core\billing\helper\PurchaseHelper;
use core\billing\item\ItemPurchaseInterface;

class PurchaseTransitionStrategy extends AbstractPurchaseStrategy
{
    /**
     * @return bool
     */
    public function canPurchase(): bool
    {
        return (bool)$this->getCurrentPurchase()
            && (bool)$this->getNeedPurchase()
            && $this->getNeedPurchase()->getPeriod() === $this->getCurrentPurchase()->getPeriod()
            && $this->getNeedPurchase()->getOfferId() !== $this->getCurrentPurchase()->getOfferId()
            && $this->getDays();
    }

    /**
     * @return ItemPurchaseInterface
     */
    public function build(): ItemPurchaseInterface
    {
        $needPurchase = $this->getNeedPurchase();

        $days = $this->getDays();

        $purchase = $this->getPurchase()
            ->setId(ItemPurchaseInterface::PURCHASE_ID_TRANSITION)
            ->setSectionId($needPurchase->getSectionId())
            ->setUnitId($needPurchase->getUnitId())
            ->setOfferId($needPurchase->getOfferId())
            ->setStartedAt(PurchaseHelper::getStartedAtNow())
            ->setFinishedAt(PurchaseHelper::getFinishedAtByDays($days))
            ->setName($needPurchase->getName())
            ->setDescription($needPurchase->getDescription())
            ->setPeriod($needPurchase->getPeriod())
            ->setDays($days)
            ->setValue($needPurchase->getValue())
            ->setPriceTransition(PurchaseHelper::getPriceLeft($this->getCurrentPurchase()));

        return $purchase;
    }

    /**
     * @return int|null
     */
    private function getDays(): ?int
    {
        $currentPurchase = $this->getCurrentPurchase();
        $needPurchase = $this->getNeedPurchase();

        if ($currentPurchase && $needPurchase) {
            $currentPriceLeft = round(PurchaseHelper::getPriceLeft($currentPurchase));

            $needPricePerDay = PurchaseHelper::getPricePerDay($needPurchase);
            $needPrice = $needPurchase->getPrice();
            $needDays = PurchaseHelper::getDays($needPurchase);

            if ($currentPriceLeft >= $needPricePerDay) {
                return round($needDays - (($needPrice - $currentPriceLeft) / $needPricePerDay));
            }
        }

        return null;
    }
}
