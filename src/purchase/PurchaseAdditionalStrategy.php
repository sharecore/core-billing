<?php

namespace core\billing\purchase;

use core\billing\helper\PurchaseHelper;
use core\billing\item\ItemPurchaseInterface;

class PurchaseAdditionalStrategy extends AbstractPurchaseStrategy
{
    /**
     * @return bool
     */
    public function canPurchase(): bool
    {
        return (bool)$this->getCurrentPurchase()
            && (bool)$this->getNeedPurchase()
            && $this->getPrice() > 0;
    }

    /**
     * @return ItemPurchaseInterface
     */
    public function build(): ItemPurchaseInterface
    {
        $needPurchase = $this->getNeedPurchase();

        $days = $this->getDays();

        $purchase = $this->getPurchase()
            ->setId(ItemPurchaseInterface::PURCHASE_ID_ADDITIONAL)
            ->setSectionId($needPurchase->getSectionId())
            ->setUnitId($needPurchase->getUnitId())
            ->setOfferId($needPurchase->getOfferId())
            ->setStartedAt(PurchaseHelper::getStartedAtNow())
            ->setFinishedAt(PurchaseHelper::getFinishedAtByDays($days))
            ->setName($needPurchase->getName())
            ->setDescription($needPurchase->getDescription())
            ->setPeriod($needPurchase->getPeriod())
            ->setDays($days)
            ->setValue($needPurchase->getValue())
            ->setPrice($this->getPrice())
            ->setCurrency($needPurchase->getCurrency())
            ->setPriceWithoutDiscount($needPurchase->getPriceWithoutDiscount())
            ->setDiscount($needPurchase->getDiscount());

        return $purchase;
    }

    /**
     * @return int|null
     */
    private function getDays(): ?int
    {
        return PurchaseHelper::getDays($this->getNeedPurchase());
    }

    /**
     * @return float|null
     */
    private function getPrice(): ?float
    {
        $currentPurchase = $this->getCurrentPurchase();
        $needPurchase = $this->getNeedPurchase();

        if ($currentPurchase) {
            $price = $needPurchase->getPrice();
            $priceLeft = PurchaseHelper::getPriceLeft($currentPurchase);
            return round($price - $priceLeft);
        }
    }
}
