<?php

namespace core\billing\purchase;

use core\billing\helper\PurchaseHelper;
use core\billing\item\ItemPurchaseInterface;

class PurchaseBuyStrategy extends AbstractPurchaseStrategy
{
    /**
     * @return bool
     */
    public function canPurchase(): bool
    {
        return true;
    }

    /**
     * @return ItemPurchaseInterface
     */
    public function build(): ItemPurchaseInterface
    {
        $needPurchase = $this->getNeedPurchase();

        $days = $this->getDays();

        $purchase = $this->getPurchase()
            ->setId(ItemPurchaseInterface::PURCHASE_ID_BUY)
            ->setSectionId($needPurchase->getSectionId())
            ->setUnitId($needPurchase->getUnitId())
            ->setOfferId($needPurchase->getOfferId())
            ->setStartedAt(PurchaseHelper::getStartedAtNow())
            ->setFinishedAt(PurchaseHelper::getFinishedAtByDays($days))
            ->setName($needPurchase->getName())
            ->setDescription($needPurchase->getDescription())
            ->setPeriod($needPurchase->getPeriod())
            ->setDays($days)
            ->setValue($needPurchase->getValue())
            ->setPrice($needPurchase->getPrice())
            ->setCurrency($needPurchase->getCurrency())
            ->setPriceWithoutDiscount($needPurchase->getPriceWithoutDiscount())
            ->setDiscount($needPurchase->getDiscount());

        return $purchase;
    }

    /**
     * @return int|null
     */
    protected function getDays(): ?int
    {
        $currentPurchase = $this->getCurrentPurchase();
        $needPurchase = $this->getNeedPurchase();

        $days = PurchaseHelper::getDays($needPurchase);

        if ($currentPurchase) {
            $days += round(PurchaseHelper::getPriceLeft($currentPurchase) / PurchaseHelper::getPricePerDay($needPurchase));
        }

        return $days;
    }
}
