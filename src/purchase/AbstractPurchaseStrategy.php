<?php

namespace core\billing\purchase;

use core\billing\item\ItemPurchaseInterface;
use core\billing\purchase\PurchaseStrategyInterface;

abstract class AbstractPurchaseStrategy implements PurchaseStrategyInterface
{
    /**
     * @var ItemPurchaseInterface
     */
    private $purchase;
    
    /**
     * @var ItemPurchaseInterface
     */
    private $needPurchase;

    /**
     * @var ItemPurchaseInterface
     */
    private $currentPurchase;

    /**
     * @param ItemPurchaseInterface $purchase
     */
    public function __construct(ItemPurchaseInterface $purchase)
    {
        $this->setPurchase($purchase);
    }

    /**
     * @param ItemPurchaseInterface $purchase
     * @return PurchaseStrategyInterface
     */
    public function setPurchase(ItemPurchaseInterface $purchase): PurchaseStrategyInterface
    {
        $this->purchase = $purchase;
        return $this;
    }

    /**
     * @return ItemPurchaseInterface
     */
    public function getPurchase(): ItemPurchaseInterface
    {
        return $this->purchase;
    }
    
    /**
     * @param ItemPurchaseInterface $purchase
     * @return PurchaseStrategyInterface
     */
    public function setNeedPurchase(ItemPurchaseInterface $purchase): PurchaseStrategyInterface
    {
        $this->needPurchase = $purchase;
        return $this;
    }

    /**
     * @return ItemPurchaseInterface
     */
    public function getNeedPurchase(): ItemPurchaseInterface
    {
        return $this->needPurchase;
    }

    /**
     * @param ItemPurchaseInterface $purchase
     * @return PurchaseStrategyInterface
     */
    public function setCurrentPurchase(ItemPurchaseInterface $purchase): PurchaseStrategyInterface
    {
        $this->currentPurchase = $purchase;
        return $this;
    }

    /**
     * @return ItemPurchaseInterface
     */
    public function getCurrentPurchase(): ?ItemPurchaseInterface
    {
        return $this->currentPurchase;
    }
}
