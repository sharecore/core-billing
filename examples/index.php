<?php


use core\billing\Payment;
use core\billing\payment\providers\yandex_money\YandexMoney;

require __DIR__ . '/define.php';

$invoice =(new \core\billing\invoice\Invoice())
    ->setId(1)
    ->setDescription('test')
    ->setEmail('av@av.av')
    ->setTotalPrice(100);

$payment = new Payment(YandexMoney::class, ['successURL' => 1]);

print_r($payment->getPayOptions($invoice));